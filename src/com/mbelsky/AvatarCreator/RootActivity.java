package com.mbelsky.AvatarCreator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

/**
 * User: mbelsky
 * Date: 25.09.13
 * Time: 20:59
 */
public class RootActivity extends Activity {

	private static final int SHOW_GALLERY_REQUEST_CODE = 0;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView( R.layout.screen_root);

		checkCameraOnDevice();

		findViewById( R.screen_root.btn_take_photo).setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				onTakePhoto();
			}
		});
		findViewById( R.screen_root.btn_open_galery).setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				onOpenGallery();
			}
		});
	}

	private void checkCameraOnDevice() {
		if ( getPackageManager().hasSystemFeature( PackageManager.FEATURE_CAMERA) ) {
			return;
		}

		final AlertDialog.Builder builder = new AlertDialog.Builder( this);
		builder.setTitle( R.string.lbl_error);
		builder.setMessage( R.string.screen_root_dialog_msg_no_camera);
		builder.setNeutralButton( R.string.lbl_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialogInterface, final int i) {
				dialogInterface.cancel();
			}
		});
		builder.setOnCancelListener( new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(final DialogInterface dialogInterface) {
				RootActivity.this.finish();
			}
		});

		builder.show();
	}

	private void onTakePhoto() {
		startActivity( new Intent( this, PhotoActivity.class));
	}

	private void onOpenGallery() {
		final Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse("content://media/internal/images/media"));
		startActivity(intent);
	}
}
