package com.mbelsky.AvatarCreator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * User: mbelsky
 * Date: 25.09.13
 * Time: 21:44
 */
public class PhotoActivity extends Activity {

	private static final int TAKE_PHOTO_REQUEST_CODE = 0;
	private static final int CROP_PHOTO_REQUEST_CODE = 1;

	private static final DateFormat FILE_NAME_DATE_FORMAT = new SimpleDateFormat( "yyyy.MM.dd_kk.mm.ss");
	private static final File PICTURES_ROOT_DIR = Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_PICTURES);
	static {
		PICTURES_ROOT_DIR.mkdirs();
	}

	private ImageView photoView;
	private View btnsContainer;
	private Button btnCrop;

	private File photoFile;
	private Uri photoUri;
	private Bitmap photoBitmap;

	private boolean isCropped;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initUi();
		hookListeners();

		startCamera();
	}

	private void initUi() {
		setContentView( R.layout.screen_photo);
		btnsContainer = findViewById( R.screen_photo.btns_container);
		btnCrop = (Button) findViewById( R.screen_photo.btn_crop);

		photoView = (ImageView)findViewById( R.screen_photo.photo);
		final ViewTreeObserver photoViewTreeObserver = photoView.getViewTreeObserver();
		photoViewTreeObserver.addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				final ViewTreeObserver obs = photoView.getViewTreeObserver();
				//JELLY BEAN
				if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ) {
					obs.removeOnGlobalLayoutListener(this);
				} else {
					obs.removeGlobalOnLayoutListener(this);
				}

				setPhotoOnBackground();
			}
		});
	}

	private void hookListeners() {
		btnCrop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				onCrop();
			}
		});
		findViewById(R.screen_photo.btn_save).setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				onSave();
			}
		});
	}

	private void startCamera() {
		final Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		photoFile = new File( PICTURES_ROOT_DIR, UUID.randomUUID().toString());
		photoUri = Uri.fromFile( photoFile);

		takePictureIntent.putExtra( MediaStore.EXTRA_OUTPUT, photoUri);
		try {
			startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST_CODE);
		} catch (ActivityNotFoundException e) {
			notHaveApplicationForTakePhoto();
		}
	}

	private void notHaveApplicationForTakePhoto() {
		final AlertDialog.Builder builder = getDefaultBuilder();
		builder.setMessage(R.string.screen_photo_dialog_msg_no_take_photo_app);
		builder.setOnCancelListener( new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(final DialogInterface dialogInterface) {
				PhotoActivity.this.finish();
			}
		});

		builder.show();
	}

	private void onCrop() {
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		cropIntent.setDataAndType( photoUri, "image/*");
		cropIntent.putExtra("crop", "true");
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		cropIntent.putExtra("outputX", 300);
		cropIntent.putExtra("outputY", 300);
		cropIntent.putExtra("return-data", true);

		try {
			startActivityForResult(cropIntent, CROP_PHOTO_REQUEST_CODE);
		} catch (ActivityNotFoundException e) {
			notHaveApplicationForCropPhoto();
		}
	}

	private void notHaveApplicationForCropPhoto() {
		final AlertDialog.Builder builder = getDefaultBuilder();
		builder.setMessage( R.string.screen_photo_dialog_msg_no_crop_photo_app);
		builder.setOnCancelListener( new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(final DialogInterface dialogInterface) {
				btnCrop.setEnabled( false);
			}
		});

		builder.show();
	}

	private void onSave() {
		final String fileName = FILE_NAME_DATE_FORMAT.format( new Date()) + ".jpg";
		final File photoFile = new File( PICTURES_ROOT_DIR, fileName);
		try {
			photoFile.createNewFile();

				if ( isCropped ) {
					FileOutputStream fos = new FileOutputStream( photoFile);
					photoBitmap.compress( Bitmap.CompressFormat.JPEG, 95, fos);
					fos.close();
			} else {
				if ( !this.photoFile.renameTo( photoFile) )
					throw new IOException();
			}

			Toast.makeText( this, R.string.screen_photo_toast_msg_photo_saved, Toast.LENGTH_SHORT).show();
		} catch (IOException e) {
			Toast.makeText( this, R.string.screen_photo_toast_msg_photo_not_saved, Toast.LENGTH_SHORT).show();
			return;
		}

		photoUri = Uri.fromFile( photoFile);
		galleryAddPhoto();
		finish();
	}

	private void galleryAddPhoto() {
		if ( null == photoUri) {
			return;
		}

		final Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(photoUri);
		sendBroadcast(mediaScanIntent);
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		switch ( requestCode ) {
			case TAKE_PHOTO_REQUEST_CODE: {
				if ( RESULT_OK == resultCode ) {
					btnsContainer.setVisibility( View.VISIBLE);
				} else {
					finish();
				}
				break;
			}
			case CROP_PHOTO_REQUEST_CODE: {
				if ( RESULT_OK == resultCode ) {
					isCropped = true;

					photoFile.delete();
					photoFile = null;
					photoUri = null;

					photoBitmap.recycle();
					btnCrop.setVisibility(View.GONE);

					final Bundle extras = data.getExtras();
					photoBitmap = extras.getParcelable("data");
					setCroppedPhotoOnBackground();
				}
			}
		}
	}

	private void setPhotoOnBackground() {
		final int targetH = photoView.getHeight();
		final int targetW = photoView.getWidth();

		final BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(photoFile.getAbsolutePath(), bmOptions);
		final int photoW = bmOptions.outWidth;
		final int photoH = bmOptions.outHeight;

		int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		photoBitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), bmOptions);
		photoBitmap = rotateBitmap( photoBitmap);
		photoView.setImageBitmap( photoBitmap);
	}

	private void setCroppedPhotoOnBackground() {
		photoView.setImageBitmap( photoBitmap);

	}

	private Bitmap rotateBitmap(final Bitmap bitmap) {
		final ExifInterface ei;
		try {
			ei = new ExifInterface( photoFile.getAbsolutePath());
		} catch (IOException ignored) {
			return bitmap;
		}

		final int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

		final float degrees;
		switch( orientation ) {
			case ExifInterface.ORIENTATION_ROTATE_90: {
				degrees = 90;
				break;
			}
			case ExifInterface.ORIENTATION_ROTATE_180: {
				degrees = 180;
				break;
			}
			case ExifInterface.ORIENTATION_ROTATE_270: {
				degrees = 270;
				break;
			}
			default:
				return bitmap;
		}

		final Matrix matrix = new Matrix();
		matrix.postRotate( degrees);
		final Bitmap rotatedBitmap = Bitmap.createBitmap( bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
		bitmap.recycle();

		try {
			final FileOutputStream fos = new FileOutputStream( photoFile);
			rotatedBitmap.compress( Bitmap.CompressFormat.JPEG, 95, fos);
			fos.close();
		} catch (IOException ignored) {
		}
		return rotatedBitmap;
	}

	private AlertDialog.Builder getDefaultBuilder() {
		final AlertDialog.Builder builder = new AlertDialog.Builder( this);
		builder.setTitle(R.string.lbl_error);
		builder.setNeutralButton( R.string.lbl_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialogInterface, final int i) {
				dialogInterface.cancel();
			}
		});

		return builder;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if ( null != photoBitmap && !photoBitmap.isRecycled() ) {
			photoBitmap.recycle();
		}
	}
}
